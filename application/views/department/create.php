<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="pull-left mt-4 mb-4 border-bottom">
            <h2>Добавить новый отдел</h2>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box box-primary mb-5">
            <form  action="/department/create" method="POST">
                <div class="box-body">
                    <div class="form-group mb-4">
                        <label for="name">Название отдела</label>
                        <input type="text" class="form-control mt-2" name="name" placeholder="Enter name">
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</div>
