<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="pull-left mt-4 mb-4 border-bottom">
            <h2>Отделы</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="/department/create">Добавить новый отдел</a>
        </div>
    </div>
    <div class="col-lg-8 ">
        <table class="table table-bordered border-dark table-hover">
            <thead>
            <tr class="table-primary">
                <th scope="col">ID</th>
                <th scope="col">Имя</th>
                <th style="width: 15%" scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($departments as $department): ?>
                <tr>
                    <td><?php echo $department["id"]?></td>
                    <td><?php echo $department["name"]?></td>
                    <td><a href="/department/delete/<?php echo $department["id"]?>" class="btn btn-danger">Удалить</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

