<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php  echo $title; ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="/public/css/main.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script src ="/public/js/jquery-3.6.0.min.js"></script>
    <script src ="/public/js/form.js"></script>
</head>
<body>
<?php require 'application/views/parts/header.php' ?>
<div class="container-fluid">
    <div class="row">
        <div class="d-flex flex-column text-white bg-primary-hard  col-lg-2 p-0" style="min-height: 100vh;">
            <?php require 'application/views/parts/sidebar.php' ?>
        </div>
    <div class="container col-lg-10 mt-2">
        <?php  echo $content; ?>
    </div>

    </div>
 </div>



</body>
</html>