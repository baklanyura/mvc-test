<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="pull-left mt-4 mb-4 border-bottom">
            <h2>Добавить нового сотрудника</h2>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box box-primary mb-5">
            <form  action="/user/create" method="POST">
                <div class="box-body">
                    <div class="form-group mb-4">
                        <label for="email">Email address </label>
                        <input type="email" class="form-control mt-2" name="email" placeholder="Enter email">
                    </div>
                    <div class="form-group mb-4">
                        <label for="name">Имя пользователя</label>
                        <input type="text" class="form-control mt-2" name="name" placeholder="Enter name">
                    </div>
                    <div class="form-group mb-4">
                        <label for="address">Адрес пользователя</label>
                        <input type="text" class="form-control mt-2" name="address" placeholder="Enter address">
                    </div>
                    <div class="form-group mb-4">
                        <label for="phone">Телефон</label>
                        <input type="text" class="form-control mt-2" name="phone" placeholder="Enter phone">
                    </div>
                    <div class="form-group mb-4">
                        <label for="comments">Коментарии</label>
                        <input type="text" class="form-control mt-2" name="comments" placeholder="Enter comments">
                    </div>
                    <div class="form-group mb-4">
                        <label for="department">Отдел</label>
                        <select name="department" class="form-select" aria-label="Default select example">
                            <option selected>Выберите значение</option>
                            <?php foreach($departments as $department): ?>
                                 <option value="<?php echo $department["id"] ?>"><?php echo $department["name"] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>

                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </form>
        </div>
    </div>
</div>
