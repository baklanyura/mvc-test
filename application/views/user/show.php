<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="pull-left mt-4 mb-4 border-bottom">
            <h2>Информация о пользователе</h2>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box box-primary mb-5">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Email address</th>
                    <td><?php echo $user['email'] ?></td>
                </tr>
                <tr>
                    <th>Имя пользователя</th>
                    <td><?php echo $user['name'] ?></td>
                </tr>
               <tr>
                    <th>Адрес пользователя</th>
                    <td><?php echo $user['address'] ?></td>
                </tr>
               <tr>
                    <th>Телефон</th>
                    <td><?php echo $user['phone'] ?></td>
                </tr>
               <tr>
                    <th>Коментарии</th>
                    <td><?php echo $user['comments'] ?></td>
                </tr>
               <tr>
                    <th>Отдел</th>
                    <td><?php echo $user['department_name'] ?></td>
               </tr>
            </table>

        </div>
    </div>
</div>
