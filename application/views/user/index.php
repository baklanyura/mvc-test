<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="pull-left mt-4 mb-4 border-bottom">
            <h2>Все пользователи</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-success" href="/user/create">Добавить нового сотрудника</a>
        </div>
    </div>
    <div class="col-lg-12">
        <table class="table table-bordered border-dark table-hover">
            <thead>
            <tr class="table-primary">
                <th scope="col">ID</th>
                <th scope="col">Имя</th>
                <th scope="col">Email</th>
                <th scope="col">Адрес</th>
                <th scope="col">Телефон</th>
                <th scope="col">Коментарии</th>
                <th scope="col">Отдел</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($users as $user): ?>
            <tr style="cursor: pointer" onclick="location.href='/user/show/<?php echo $user["id"]; ?>'">
                <td><?php echo $user["id"]?></td>
                <td><?php echo $user["name"]?></td>
                <td><?php echo $user["email"]?></td>
                <td><?php echo $user["address"]?></td>
                <td><?php echo $user["phone"]?></td>
                <td><?php echo $user["comments"]?></td>
                <td><?php echo $user["department_name"]?></td>

            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

