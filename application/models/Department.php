<?php
namespace application\models;

use application\core\Model;

class Department extends Model
{
    public $error;

    public function getDepartments()
    {
        $departments =$this->db->row('SELECT * FROM departments');
        return $departments;
    }

    public function uniqueDepartmentNameValidate($name)
    {
        $departments = $this->getDepartments();
        foreach ($departments as $department){
            if($department['name'] == $name){
                return false;
            }
        }
        return true;
    }

    public function storeValidate($post){
        $nameLen = iconv_strlen($post['name']);
        if(!$this->uniqueDepartmentNameValidate($post['name'])){
            $this->error = "Такое название отдела уже есть, введите другое название";
            return false;
        }elseif ($nameLen < 1 or $nameLen > 50)
        {
            $this->error = "Поле название отдела должно содержать от 1 до 50 символов";
            return false;
        }
        return true ;
    }

    public function createDepartment($post){
        $sql = "INSERT INTO `departments` (`name`) VALUES (:name)";
        $params =[
            ':name'=> $post['name'],
        ];
        $this->db->query($sql, $params);
        return $this->db->lastInsertId();
    }

    public function isDepartmentExist($id){
        $sql = "SELECT id FROM departments WHERE id = :id";
        $params = [
            ':id' =>$id
        ];
        return $this->db->column($sql,$params);
    }

    public function departmentDelete($id){
        $sql = "DELETE FROM departments WHERE id = :id";
        $params = [
            ':id' =>$id
        ];
        return $this->db->query($sql,$params);
    }
}