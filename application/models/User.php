<?php
namespace application\models;

use application\core\Model;

class User extends Model
{
    public $error;

    public function getUsers()
    {
        $result =$this->db->row('SELECT * FROM users');
        return $result;
    }

    public function getUsersWithDepartment(){
        $all_users = $this->getUsers();
        $users_new =[];
        foreach ($all_users as $user){
            $user['department_name'] = $this->findUserDepartmentById($user['id']);
            $users_new[] = $user;
        }
        return $users_new;
    }

    public function uniqueUsersEmailValidate($email)
    {
        $users = $this->getUsers();
        foreach ($users as $user){
            if($user['email'] == $email){
                return false;
            }
        }
        return true;
    }


    public function storeValidate($post){
        $mail = filter_var($post['email'], FILTER_VALIDATE_EMAIL);
        $nameLen = iconv_strlen($post['name']);
        $addressLen = iconv_strlen($post['address']);
        $phoneInt = filter_var($post['phone'], FILTER_VALIDATE_INT);
        $commentsLen = iconv_strlen($post['comments']);
        if (!$mail)
        {
            $this->error = "Поле должно содержать Email";
            return false;
        }elseif (!$this->uniqueUsersEmailValidate($post['email']))
        {
            $this->error = "Такая почта уже используется, введите другую почту";
            return false;
        }elseif ($nameLen < 3 or $nameLen > 50)
        {
            $this->error = "Имя пользователя должно содержать от 5 до 50 символов";
            return false;
        }elseif ($addressLen < 3 or $addressLen > 50)
        {
            $this->error = "Адресс пользователя должно содержать от 3 до 50 символов";
            return false;
        }elseif (!$phoneInt)
        {
            $this->error = "Телефон должен быть числом";
            return false;
        }elseif ($commentsLen < 3 or $commentsLen > 500)
        {
            $this->error = "Коментарии должны содержать от 3 до 500 символов";
            return false;
        }
        return true ;
    }

    public function createUser($post){
        $sql = "INSERT INTO `users` (`email`,`name`,`address`,`phone`,`comments`,`department_id`) VALUES (:email, :name, :address, :phone, :comments, :department_id)";
        $params =[
            ':email'=> $post['email'],
            ':name'=> $post['name'],
            ':address'=> $post['address'],
            ':phone'=> $post['phone'],
            ':comments'=> $post['comments'],
            ':department_id'=> $post['department'],
        ];

        $this->db->query($sql, $params);
        return $this->db->lastInsertId();
    }

    public function isUserExist($id){
        $sql = "SELECT id FROM users WHERE id = :id";
        $params = [
            ':id' =>$id
        ];
        return $this->db->column($sql,$params);
    }

    public function findByIdUser($id){
        $sql = "SELECT * FROM users WHERE id = :id";
        $params = [
            ':id' =>$id
        ];
        $user = $this->db->row($sql,$params);
        return $user[0];
    }

    public function findUserDepartmentById($id){

        $user = $this->findByIdUser($id);
        $sql = "SELECT name FROM departments WHERE id = :id";
        $params = [
            ':id' =>$user['department_id']
        ];
        return $this->db->column($sql,$params);
    }
}