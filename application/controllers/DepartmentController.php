<?php
namespace application\controllers;

use application\core\Controller;
use application\lib\Db;
use application\models\Department;
use application\models\User;

class  DepartmentController extends Controller
{
    public function __construct($route) {
        parent::__construct($route);
    }

     public function indexAction(){
         $departments = new Department;
         $departments = $departments->getDepartments();
         $compact = [
             'departments' => $departments,
         ];

        $this->view->render('Отделы', $compact);
     }

    public function createAction(){
        if (!empty($_POST)) {
            $department = new Department;
            if (!$department->storeValidate($_POST)) {
                $this->view->message('error', $department->error);
            }
            $new_department = $department->createDepartment($_POST);
            if (!$new_department) {
                $this->view->message('error', 'Ошибка обработки запроса');
            }
            $this->view->location('/department/index');
        }

        $this->view ->render('Создать отдел');
    }

    public function deleteAction(){
        $department = new Department;
        if(!$department->isDepartmentExist($this->route['id'])){
            $this->view->errorCode(404);
        }
        if(!$department->departmentDelete($this->route['id'])){
            $this->view->message('error', 'Ошибка обработки запроса');
        }
        $this->view->redirect('/department/index');
    }
}