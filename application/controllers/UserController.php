<?php
namespace application\controllers;

use application\core\Controller;
use application\lib\Db;
use application\models\Department;
use application\models\User;

class  UserController extends Controller
{

    public function __construct($route) {
        parent::__construct($route);
    }

     public function indexAction(){
         $user = new User;
         $users = $user->getUsersWithDepartment();
         $compact = [
             'users' => $users,
         ];
        $this->view ->render('Все пользователи', $compact );
     }

    public function createAction(){

        if (!empty($_POST)) {
            $user = new User;
            if (!$user->storeValidate($_POST)) {
                $this->view->message('error', $user->error);
            }
            $new_user = $user->createUser($_POST);
            if (!$new_user) {
                $this->view->message('error', 'Ошибка обработки запроса');
            }
            $this->view->location('/user/index');
        }
        $departments = new Department;
        $departments= $departments->getDepartments();

        $compact = [
            'departments' => $departments
        ];

        $this->view ->render('Добавить нового пользователя', $compact);
    }


    public function showAction(){
        $users = new User;
        $user = $users->findByIdUser($this->route['id']);
        if(!$user){
            $this->view->errorCode(404);
        }
        $user['department_name'] = $users->findUserDepartmentById($this->route['id']);
        $compact = [
            'user' => $user
        ];
        $this->view ->render('Просмотр пользователя', $compact);
    }

}