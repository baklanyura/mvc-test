<?php

return [
    '' => [
        'controller' => 'user',
        'action' => 'index',
    ],

    //User
     'user/index' => [
            'controller' => 'user',
            'action' => 'index',
        ],
     'user/create' => [
            'controller' => 'user',
            'action' => 'create',
        ],
     'user/show/{id:\d+}' => [
            'controller' => 'user',
            'action' => 'show',
        ],
    //Department
     'department/index' => [
            'controller' => 'department',
            'action' => 'index',
        ],
     'department/create' => [
            'controller' => 'department',
            'action' => 'create',
        ],
     'department/delete/{id:\d+}' => [
            'controller' => 'department',
            'action' => 'delete',
        ],
];